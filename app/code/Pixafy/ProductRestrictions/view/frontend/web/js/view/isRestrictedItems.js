define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Pixafy_ProductRestrictions/js/model/isRestrictedItems'
    ],
    function (Component, additionalValidators, restrictedValidation) {
        'use strict';
        additionalValidators.registerValidator(restrictedValidation);
        return Component.extend({});
    }
);