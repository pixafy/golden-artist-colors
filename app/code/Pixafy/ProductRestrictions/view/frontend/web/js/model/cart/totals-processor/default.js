define([
    'jquery',
    'underscore',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Checkout/js/model/resource-url-manager',
    'Magento_Checkout/js/model/quote',
    'mage/storage',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/cart/cache',
    'Magento_Customer/js/customer-data'
], function ($, _, getTotalsAction, resourceUrlManager, quote, storage, totalsService, errorProcessor, cartCache, customerData) {
    'use strict';

    /**
     * Update cart.
     */
    var updateCart = function() {
        var form = $('form#form-validate');
        $.ajax({
            url: window.location.href,
            data: form.serialize(),
            showLoader: true,
            success: function (res) {
                var parsedResponse = $.parseHTML(res);
                var result = $(parsedResponse).find("#form-validate");
                var sections = ['cart'];
                $("#form-validate").replaceWith(result);
                customerData.reload(sections, true);
                var errors = $(".cart.item.message.error");
                if (!errors.length) {
                    window.checkoutConfig.quoteMessagesCustom = false;
                } else {
                    window.checkoutConfig.quoteMessagesCustom = true;
                }
                watchProceedToCheckout();
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.Message);
                watchProceedToCheckout();
            }
        });
    }

    /**
     * Disabling proceed to checkout button if quote has error items
     */
    var watchProceedToCheckout = function () {
        var dataEstimateValues = $('#shipping-zip-form').serializeArray(),
            enable = true;
        if (dataEstimateValues.length > 0) {
            dataEstimateValues.forEach((item) => {
                if ((item.value === undefined || item.value === null || item.value === '') && item.name !== 'region' && item.name !== 'region_id') {
                    enable = false;
                }
            });
        } else {
            enable = false;
        }

        if (enable &&
            window.checkoutConfig &&
            window.checkoutConfig.quoteMessages &&
            window.checkoutConfig.quoteMessages &&
            typeof window.checkoutConfig.quoteMessages === 'object' &&
            Object.values(window.checkoutConfig.quoteMessages).length > 0
        ) {
            for (const quoteItem in window.checkoutConfig.quoteMessages) {
                if (window.checkoutConfig.quoteMessages[quoteItem] !== '') {
                    enable = false;
                    break;
                }
            }
        }

        if (window.checkoutConfig.quoteMessagesCustom === true) {
            enable = false;
        } else if (window.checkoutConfig.quoteMessagesCustom === false){
            enable = true;
        }
        window.checkoutConfig.quoteMessagesCustom = null;

        $("button[data-role='proceed-to-checkout']").removeClass('disabled');
        $("button[data-role='proceed-to-checkout']").removeAttr('disabled');
        $(".action.primary.checkout").attr('disabled', false);
        if (enable === false) {
            $(".action.primary.checkout").attr('disabled', true);
            $("button[data-role='proceed-to-checkout']").addClass('disabled');
            $("button[data-role='proceed-to-checkout']").attr('disabled', true);
        }
    }

    /**
     * Load data from server.
     *
     * @param {Object} address
     */
    var loadFromServer = function (address) {
        var serviceUrl,
            payload;

        // Start loader for totals block
        totalsService.isLoading(true);
        serviceUrl = resourceUrlManager.getUrlForTotalsEstimationForNewAddress(quote);
        payload = {
            addressInformation: {
                address: _.pick(address, cartCache.requiredFields)
            }
        };

        if (quote.shippingMethod() && quote.shippingMethod()['method_code']) {
            payload.addressInformation['shipping_method_code'] = quote.shippingMethod()['method_code'];
            payload.addressInformation['shipping_carrier_code'] = quote.shippingMethod()['carrier_code'];
        }

        return storage.post(
            serviceUrl, JSON.stringify(payload), false
        ).done(function (result) {
            var data = {
                totals: result,
                address: address,
                cartVersion: customerData.get('cart')()['data_id'],
                shippingMethodCode: null,
                shippingCarrierCode: null
            };

            if (quote.shippingMethod() && quote.shippingMethod()['method_code']) {
                data.shippingMethodCode = quote.shippingMethod()['method_code'];
                data.shippingCarrierCode = quote.shippingMethod()['carrier_code'];
            }

            quote.setTotals(result);
            cartCache.set('cart-data', data);

            // checking and updating cart
            var checkCart = true;
            var dataEstimateValues = $('#shipping-zip-form').serializeArray();
            dataEstimateValues.forEach((element) => {
                if ((element.value === undefined || element.value === null || element.value === '') && element.name !== 'region' && element.name !== 'region_id') {
                    checkCart = false;
                }
            });
            if (checkCart === true) {
                updateCart();
            }
        }).fail(function (response) {
            errorProcessor.process(response);
        }).always(function () {
            // Stop loader for totals block
            totalsService.isLoading(false);
        });
    };

    return {
        /**
         * Array of required address fields.
         * @property {Array.String} requiredFields
         * @deprecated Use cart cache.
         */
        requiredFields: cartCache.requiredFields,

        /**
         * Get shipping rates for specified address.
         * @param {Object} address
         */
        estimateTotals: function (address) {
            var data = {
                shippingMethodCode: null,
                shippingCarrierCode: null
            };

            if (quote.shippingMethod() && quote.shippingMethod()['method_code']) {
                data.shippingMethodCode = quote.shippingMethod()['method_code'];
                data.shippingCarrierCode = quote.shippingMethod()['carrier_code'];
            }

            return loadFromServer(address);
        }
    };
});
