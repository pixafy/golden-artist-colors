define([
        'jquery',
        'mage/translate',
        'mage/validation',
        'Magento_Ui/js/model/messageList'
    ], function ($, $t, validation, messageList) {
        'use strict';

        return {

            /**
             * Validate checkout agreements
             *
             * @returns {Boolean}
             */
            validate: function () {
                var item_errors_flag = false;
                $.each(window.checkoutConfig.quoteMessages, function(i, item) {
                    if(item && item !== ''){
                        item_errors_flag = true;
                    }
                });
                if(item_errors_flag){
                    messageList.addErrorMessage({ message: $t('You have some error items or restricted items in your cart. Please delete them before placing an order') });
                    return !item_errors_flag;
                }
                else{
                    return true;
                }
            }
        };
    }
);
