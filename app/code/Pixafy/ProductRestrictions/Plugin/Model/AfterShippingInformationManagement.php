<?php

namespace Pixafy\ProductRestrictions\Plugin\Model;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\Exception\RuntimeException;

class AfterShippingInformationManagement
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param $result
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return array
     * @throws RuntimeException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSaveAddressInformation(
        ShippingInformationManagement $subject,
        $result,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $quote = $this->checkoutSession->getQuote();
        if ($quote->getHasError()) {
            throw new RuntimeException(
                __('You have some error items or restricted items in your cart for selected address. Please check shopping cart before placing an order.')
            );
        }

        foreach ($quote->getAllVisibleItems() as $visibleItem)
        {
            if ($visibleItem->getHasError() || $visibleItem->getErrorInfos()) {
                throw new RuntimeException(
                    __('You have some error items or restricted items in your cart for selected address. Please check shopping cart before placing an order.')
                );
            }
        }
        return $result;
    }
}
