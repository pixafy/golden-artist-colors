<?php

namespace Pixafy\ProductRestrictions\Plugin\Model;

use Magento\Quote\Api\CartRepositoryInterface;

class ShippingMethodManagement
{
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    public function afterEstimateByExtendedAddress($subject, $result, $cartId, $address)
    {
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->getShippingAddress()->save();
        return $result;
    }
}
