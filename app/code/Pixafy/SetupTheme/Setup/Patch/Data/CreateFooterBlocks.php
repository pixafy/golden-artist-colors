<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\SetupTheme\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class CreateFooterBlocks
 * @since 1.0.0
 */
class CreateFooterBlocks implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Cms\Model\GetBlockByIdentifier
     */
    private $getBlockByIdentifierCommand;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $blockFactory;

    /**
     * @var \Magento\Cms\Api\BlockRepositoryInterface
     */
    private $blockRepository;

    /**
     * CreateFooterBlocks constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\GetBlockByIdentifier $getBlockByIdentifierCommand
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param \Magento\Cms\Api\BlockRepositoryInterface $blockRepository
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\GetBlockByIdentifier $getBlockByIdentifierCommand,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Api\BlockRepositoryInterface $blockRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->getBlockByIdentifierCommand = $getBlockByIdentifierCommand;
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - then under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $storeId = 0;

        foreach ($this->getBlocks() as $data) {
            $identifier = $data['identify'];
            try {
                /** @var \Magento\Cms\Api\Data\BlockInterface $block */
                $block = $this->getBlockByIdentifierCommand->execute($identifier, $storeId);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                /** @var \Magento\Cms\Api\Data\BlockInterface $block */
                $block = $this->blockFactory->create();
                $block->setTitle($data['title']);
                $block->setIdentifier($identifier);
                $block->setIsActive(true);
            }
            $block->setContent($data['content']);
            $this->blockRepository->save($block);
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * Example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get blocks
     */
    private function getBlocks()
    {
        return [
            'footer_block_left' => [
                'identify' => 'footer_block_left',
                'title' => 'Footer block left',
                'content' => '<div data-content-type="html" data-appearance="default" data-element="main">&lt;h3 class="title"&gt;Our brands&lt;/h3&gt;
&lt;ul&gt;
    &lt;li&gt;&lt;a href="https://goldenpaints.com/"&gt;Golden Paints&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="https://www.goldenpaintworks.com/"&gt;Golden Paintworks&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="https://www.qorcolors.com/"&gt;QoR Watercolors&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="https://www.williamsburgoils.com/"&gt;Williamsburg Oils&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</div>'
            ],
            'footer_block_right' => [
                'identify' => 'footer_block_right',
                'title' => 'Footer block right',
                'content' => '<div data-content-type="html" data-appearance="default" data-element="main">&lt;h3 class="title"&gt;About Us&lt;/h3&gt;
&lt;ul&gt;
    &lt;li&gt;&lt;a href="{{store url="privacy-policy-cookie-restriction-mode"}}"&gt;Privacy police&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href="{{store url="contact"}}"&gt;Contact Us&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</div>'
            ],
        ];
    }
}
