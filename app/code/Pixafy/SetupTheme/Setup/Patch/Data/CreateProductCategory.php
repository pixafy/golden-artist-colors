<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\SetupTheme\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class CreateProductCategory
 * @since 1.0.0
 */
class CreateProductCategory implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Cms\Model\GetPageByIdentifier
     */
    private $getPageByIdentifierCommand;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * CreateProductCategory constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface $pageRepository
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->getPageByIdentifierCommand = $getPageByIdentifierCommand;
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - then under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $identifier = 'product-category';
        $storeId = 0;

        try {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->getPageByIdentifierCommand->execute($identifier, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->pageFactory->create()
                ->setData([
                    'title' => 'Product Category',
                    'page_layout' => 'cms-full-width',
                    'meta_keywords' => 'Product Category',
                    'meta_description' => 'Product Category',
                    'identifier' => $identifier,
                    'content_heading' => '',
                    'layout_update_xml' => '',
                    'url_key' => $identifier,
                    'is_active' => 1,
                    'stores' => [0]
                ]);
        }
        $page->setContent($this->getContent());
        $page->setContentHeading('');
        $this->pageRepository->save($page);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * Example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get content
     *
     * @return string
     */
    private function getContent()
    {
        return '<style>#html-body [data-pb-style=UHDJQSP]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=PR7PKV6]{text-align:center}#html-body [data-pb-style=BYJIC4M]{background-position:left top}#html-body [data-pb-style=BYJIC4M],#html-body [data-pb-style=TN4RN2Q],#html-body [data-pb-style=WQ89SI8]{justify-content:flex-start;display:flex;flex-direction:column;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=WQ89SI8]{background-position:center bottom;text-align:center;min-height:300px;width:100%;align-self:stretch}#html-body [data-pb-style=TN4RN2Q]{background-position:left top}#html-body [data-pb-style=F37078J]{margin-top:35px;margin-bottom:25px}#html-body [data-pb-style=AEIGO9H]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=HI3YYWG],#html-body [data-pb-style=MFCJXCH],#html-body [data-pb-style=MJFEAXM]{justify-content:center;display:flex;flex-direction:column;background-position:center center;background-size:contain;background-repeat:no-repeat;background-attachment:scroll;text-align:center;min-height:300px;width:33.3333%;align-self:center}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="UHDJQSP"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="PR7PKV6">Paint Products</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="BYJIC4M"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="full-height" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/Heavy_Body_8oz_4.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/Heavy_Body_8oz_4_1.jpg}}\&quot;}" data-element="main" data-pb-style="WQ89SI8"></div></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="TN4RN2Q"><div data-content-type="text" data-appearance="default" data-element="main" data-pb-style="F37078J"><p style="text-align: center;">Shop for paint products here.</p></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="AEIGO9H"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;}" data-element="main" data-pb-style="MFCJXCH"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;}" data-element="main" data-pb-style="HI3YYWG"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077_2.jpg}}\&quot;}" data-element="main" data-pb-style="MJFEAXM"></div></div></div></div>';
    }
}
