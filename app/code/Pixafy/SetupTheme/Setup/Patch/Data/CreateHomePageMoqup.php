<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\SetupTheme\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class CreateHomePageMoqup
 * @since 1.0.0
 */
class CreateHomePageMoqup implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Cms\Model\GetPageByIdentifier
     */
    private $getPageByIdentifierCommand;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * CreateHomePageMoqup constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface $pageRepository
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->getPageByIdentifierCommand = $getPageByIdentifierCommand;
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - then under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $identifier = 'home-page-moqup';
        $storeId = 0;

        try {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->getPageByIdentifierCommand->execute($identifier, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->pageFactory->create()
                ->setData([
                    'title' => 'Home Page moqup',
                    'page_layout' => 'cms-full-width',
                    'meta_keywords' => 'Home Page moqup',
                    'meta_description' => 'Home Page moqup',
                    'identifier' => $identifier,
                    'content_heading' => '',
                    'layout_update_xml' => '',
                    'url_key' => $identifier,
                    'is_active' => 1,
                    'stores' => [0]
                ]);
        }
        $page->setContent($this->getContent());
        $this->pageRepository->save($page);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * Example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get content
     *
     * @return string
     */
    private function getContent()
    {
        return '<style>#html-body [data-pb-style=DNL3IG1]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=UK7B9L6]{text-align:center}#html-body [data-pb-style=PEXM6GP]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=UKAUJ3M]{min-height:300px}#html-body [data-pb-style=FWVA9VH]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=I743PJF]{min-height:300px;background-color:transparent}#html-body [data-pb-style=YBYDCUJ]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=KR6KTPT]{min-height:300px;background-color:transparent}#html-body [data-pb-style=BL00OTO]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=GHSOEOD]{min-height:300px;background-color:transparent}#html-body [data-pb-style=NEG7C54]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=P4S9YFN]{min-height:300px;background-color:transparent}#html-body [data-pb-style=TUI5XSG]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=A0HJGXI]{min-height:300px;background-color:transparent}#html-body [data-pb-style=EWKTWRS]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=JV9P2RW]{margin-top:10px;margin-bottom:10px}#html-body [data-pb-style=AYW3SDA]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll;min-height:350px}#html-body [data-pb-style=EJ03E5A],#html-body [data-pb-style=TK26DHK],#html-body [data-pb-style=XD27LUR]{text-align:center}#html-body [data-pb-style=EJ03E5A],#html-body [data-pb-style=EUH6T5P],#html-body [data-pb-style=TK26DHK],#html-body [data-pb-style=XD27LUR]{justify-content:center;display:flex;flex-direction:column;background-position:center center;background-size:contain;background-repeat:no-repeat;background-attachment:scroll;min-height:300px;width:25%;align-self:center}#html-body [data-pb-style=NXESN9D]{width:100%;border-width:1px;border-color:#cecece;display:inline-block}#html-body [data-pb-style=DMR3ISC]{text-align:center}#html-body [data-pb-style=K02AG7L],#html-body [data-pb-style=QAC8M6H],#html-body [data-pb-style=VE8Y28K]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=K02AG7L],#html-body [data-pb-style=QAC8M6H]{justify-content:center;background-position:center center;background-size:contain;text-align:center;min-height:300px;width:50%;align-self:center}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="DNL3IG1"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="UK7B9L6">welcome to shopGOLDEN.com</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="PEXM6GP"><div class="pagebuilder-slider" data-content-type="slider" data-appearance="default" data-autoplay="false" data-autoplay-speed="4000" data-fade="false" data-infinite-loop="false" data-show-arrows="false" data-show-dots="true" data-element="main" data-pb-style="UKAUJ3M"><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/GOLDEN_Bright_Gold_Jar_1.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/GAC_Gold.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="FWVA9VH"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="I743PJF"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/ColorChartPainting_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/ColorChartPainting_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="YBYDCUJ"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="KR6KTPT"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/mural_paints_orange_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/mural_paints_orange_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="BL00OTO"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="GHSOEOD"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/QoR_insitu_01_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/QoR_insitu_01_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="NEG7C54"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="P4S9YFN"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/UltrablueMill_01.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/UltrablueMill_01_1.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="TUI5XSG"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="A0HJGXI"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="EWKTWRS"><div data-content-type="text" data-appearance="default" data-element="main" data-pb-style="JV9P2RW"><p style="text-align: center;">Welcome to shopGOLDEN.com. We\'re glad you\'re here!</p>
<p style="text-align: center;">Shop our Merch, Studio Tools, and the complete line of GOLDEN Paintworks offerings.</p></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="AYW3SDA"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/Filling_Yellow.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/Filling_Yellow.jpg}}\&quot;}" data-element="main" data-pb-style="TK26DHK"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/Wmsbrg_exptubes_01_3.jpg}}\&quot;}" data-element="main" data-pb-style="XD27LUR"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/Tube_Cap_3.jpg}}\&quot;}" data-element="main" data-pb-style="EJ03E5A"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/IMG_0077.jpg}}\&quot;}" data-element="main" data-pb-style="EUH6T5P"></div></div><div data-content-type="divider" data-appearance="default" data-element="main"><hr data-element="line" data-pb-style="NXESN9D"></div><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="DMR3ISC">featured products</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="VE8Y28K"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/Bronze_web-e1587592087694.jpg}}\&quot;}" data-element="main" data-pb-style="QAC8M6H"></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/GPW-slow-dry-fluid-acrylics-e1586984421226.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/GPW-slow-dry-fluid-acrylics-e1586984421226.jpg}}\&quot;}" data-element="main" data-pb-style="K02AG7L"></div></div></div></div>';
    }
}
