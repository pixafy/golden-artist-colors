<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\SetupTheme\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class UpdateHomePageMoqup
 * @since 1.0.0
 */
class UpdateHomePageMoqup implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Cms\Model\GetPageByIdentifier
     */
    private $getPageByIdentifierCommand;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * UpdateHomePageMoqup constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface $pageRepository
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\GetPageByIdentifier $getPageByIdentifierCommand,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->getPageByIdentifierCommand = $getPageByIdentifierCommand;
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - then under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $identifier = 'home-page-moqup';
        $storeId = 0;

        try {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->getPageByIdentifierCommand->execute($identifier, $storeId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            /** @var \Magento\Cms\Api\Data\PageInterface $page */
            $page = $this->pageFactory->create()
                ->setData([
                    'title' => 'Golden Artist Colors',
                    'page_layout' => '1column',
                    'meta_keywords' => 'Home Page moqup',
                    'meta_description' => 'Home Page moqup',
                    'identifier' => $identifier,
                    'content_heading' => '',
                    'layout_update_xml' => '',
                    'url_key' => $identifier,
                    'is_active' => 1,
                    'stores' => [0]
                ]);
        }
        $page->setTitle('Golden Artist Colors');
        $page->setPageLayout('1column');
        $page->setContent($this->getContent());
        $this->pageRepository->save($page);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * Example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [
            \Pixafy\SetupTheme\Setup\Patch\Data\CreateHomePageMoqup::class
        ];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get content
     *
     * @return string
     */
    private function getContent()
    {
        return '<style>#html-body [data-pb-style=D1A4KB0]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=Q98VHSW]{text-align:left}#html-body [data-pb-style=V20VFDH]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=IJXL7VO]{min-height:300px}#html-body [data-pb-style=PAT26RO]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=J75HOKU]{min-height:300px;background-color:transparent}#html-body [data-pb-style=JA6INDM]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=JO2WWIP]{min-height:300px;background-color:transparent}#html-body [data-pb-style=YGL7RDS]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=Q85GOXF]{min-height:300px;background-color:transparent}#html-body [data-pb-style=YLHBUE1]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:300px}#html-body [data-pb-style=PE1RFBH]{min-height:300px;background-color:transparent}#html-body [data-pb-style=CT81S7E]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=JHN23IP]{text-align:center}#html-body [data-pb-style=KL09RYD],#html-body [data-pb-style=V2MN5UC]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll;min-height:350px}#html-body [data-pb-style=V2MN5UC]{justify-content:center;background-position:center center;background-size:contain;text-align:center;min-height:300px;width:33.3333%;margin-bottom:10px;align-self:center}#html-body [data-pb-style=TNGN8E2]{display:inline-block}#html-body [data-pb-style=ESCWYF1]{text-align:center}#html-body [data-pb-style=HXQFB54]{justify-content:center;display:flex;flex-direction:column;background-position:center center;background-size:contain;background-repeat:no-repeat;background-attachment:scroll;text-align:center;min-height:300px;width:33.3333%;margin-bottom:10px;align-self:center}#html-body [data-pb-style=E14DVIJ]{display:inline-block}#html-body [data-pb-style=NYGGOMX]{text-align:center}#html-body [data-pb-style=NH8UMPK]{justify-content:center;display:flex;flex-direction:column;background-position:center center;background-size:contain;background-repeat:no-repeat;background-attachment:scroll;min-height:300px;width:33.3333%;align-self:center}#html-body [data-pb-style=K3DNU8F]{text-align:center}#html-body [data-pb-style=PUHKU1L]{display:inline-block}#html-body [data-pb-style=LW2EQBN],#html-body [data-pb-style=SJ4JHWF]{text-align:center}#html-body [data-pb-style=OXU73DA],#html-body [data-pb-style=V7AXATR]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=V7AXATR]{text-align:center}#html-body [data-pb-style=OXU73DA]{width:50%;margin-right:0;align-self:stretch}#html-body [data-pb-style=MHKWDVJ]{width:50%;align-self:stretch}#html-body [data-pb-style=E36HRIK],#html-body [data-pb-style=MHKWDVJ]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=S6TLNDU]{text-align:center}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="D1A4KB0"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="Q98VHSW">Welcome to shop.GoldenArtistColors.com</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="V20VFDH"><div class="pagebuilder-slider" data-content-type="slider" data-appearance="default" data-autoplay="true" data-autoplay-speed="4000" data-fade="true" data-infinite-loop="true" data-show-arrows="true" data-show-dots="false" data-element="main" data-pb-style="IJXL7VO"><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/orange_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/orange_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="PAT26RO"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="J75HOKU"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/magenta_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/magenta_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="JA6INDM"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="JO2WWIP"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/mural_buckets.png}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/mural_buckets_1.png}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="YGL7RDS"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="Q85GOXF"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/GACHB-Naphredmill_1400x480_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/GACHB-Naphredmill_1400x480_3.jpg}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="YLHBUE1"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="PE1RFBH"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="CT81S7E"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="JHN23IP">&nbsp;Merchandise, Studio Tools, and Golden Paintworks Products - All in One Place.</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="KL09RYD"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/SF-YellowGrn_1280x720_1.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/SF-YellowGrn_1280x720_2.jpg}}\&quot;}" data-element="main" data-pb-style="V2MN5UC"><div data-content-type="buttons" data-appearance="inline" data-same-width="true" data-element="main"><div data-content-type="button-item" data-appearance="default" data-element="main" data-pb-style="TNGN8E2"><a class="pagebuilder-button-primary" href="https://mcstaging-shop.goldenartistcolors.com/merchandise.html" target="" data-link-type="default" data-element="link" data-pb-style="ESCWYF1"><span data-element="link_text">&nbsp;Merchandise</span></a></div></div></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/brushes_2200x1208_2.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/brushes_2200x1208_3.jpg}}\&quot;}" data-element="main" data-pb-style="HXQFB54"><div data-content-type="buttons" data-appearance="inline" data-same-width="false" data-element="main"><div data-content-type="button-item" data-appearance="default" data-element="main" data-pb-style="E14DVIJ"><a class="pagebuilder-button-primary" href="https://mcstaging-shop.goldenartistcolors.com/studio-tools.html" target="" data-link-type="default" data-element="link" data-pb-style="NYGGOMX"><span data-element="link_text">Studio Tools</span></a></div></div></div><div class="pagebuilder-column" data-content-type="column" data-appearance="align-center" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/mural_red.jpg}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/mural_red_1.jpg}}\&quot;}" data-element="main" data-pb-style="NH8UMPK"><div data-content-type="buttons" data-appearance="inline" data-same-width="false" data-element="main" data-pb-style="K3DNU8F"><div data-content-type="button-item" data-appearance="default" data-element="main" data-pb-style="PUHKU1L"><a class="pagebuilder-button-primary" href="https://mcstaging-shop.goldenartistcolors.com/architecturalpaints.html" target="" data-link-type="default" data-element="link" data-pb-style="LW2EQBN"><span data-element="link_text">Architectural Paints</span></a></div></div></div></div><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="SJ4JHWF">Who is Golden Artist Colors?</h2></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div class="with-margin" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="V7AXATR"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column margin-right" data-content-type="column" data-appearance="full-height" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/factory_01_2.jpg}}\&quot;}" data-element="main" data-pb-style="OXU73DA"></div><div class="pagebuilder-column margin-left" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="MHKWDVJ"><div data-content-type="text" data-appearance="default" data-element="main"><p class="MsoNormal" style="text-align: left;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif;">Golden Artist Colors, Inc. was founded in a cow barn in 1980 by Sam and Adele Golden, along with their son Mark and his wife Barbara. Since then, the company has grown to employ over 200 individuals and currently has two locations, a 100,000-square-foot facility in rural Columbus, N.Y., and a 45,000-square-foot commercial warehouse and distribution center in Norwich, N.Y. A manufacturer of artist quality materials sold across the globe, Golden Artist Colors manufactures GOLDEN Acrylics, Williamsburg Oils and QoR<sup>®</sup> Watercolor. </span></p>
<p class="MsoNormal" style="text-align: left;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif;">Also part of the company is Golden Paintworks, the company’s architectural brand. Furthermore, the Custom Lab at GOLDEN serves artists and organizations by formulating materials to fit their specific needs and purposes. Not only committed to its customers and local community, GOLDEN is dedicated to its employees as well. </span></p>
<p class="MsoNormal" style="text-align: left;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif;">In May 2010, GOLDEN began its employee ownership journey with staff and on October 1, 2021 a new phase in the journey began when GOLDEN became a 100% Employee-Owned company!</span></p>
<p class="MsoNormal" style="text-align: left;"><span style="font-size: 10.0pt; font-family: Arial,sans-serif;">We hope you find value in supporting Golden Artist Colors, a business with modest beginnings that treasures the value of employee contributions in daily operations!</span></p></div></div></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="E36HRIK"><h2 data-content-type="heading" data-appearance="default" data-element="main">Featured Products&nbsp;</h2><div data-content-type="products" data-appearance="carousel" data-autoplay="false" data-autoplay-speed="4000" data-infinite-loop="false" data-show-arrows="false" data-show-dots="true" data-carousel-mode="default" data-center-padding="90px" data-element="main" data-pb-style="S6TLNDU">{{widget type="Magento\CatalogWidget\Block\Product\ProductsList" template="Magento_PageBuilder::catalog/product/widget/content/carousel.phtml" anchor_text="" id_path="" show_pager="0" products_count="5" condition_option="category_ids" condition_option_value="41" type_name="Catalog Products Carousel" conditions_encoded="^[`1`:^[`aggregator`:`all`,`new_child`:``,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`value`:`1`^],`1--1`:^[`operator`:`==`,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`value`:`41`^]^]" sort_order="position"}}</div></div></div>';
    }
}
