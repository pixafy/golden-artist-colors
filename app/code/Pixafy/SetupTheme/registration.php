<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Pixafy_SetupTheme', __DIR__);
