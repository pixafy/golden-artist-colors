<?php

namespace Pixafy\Rewrite\Plugin\Block\Form\Renderer;

use Magento\CustomAttributeManagement\Block\Form\Renderer\Multiselect;

/**
 * Plugin class to avoid deprecation
 * error on customer create account page
 */
class MultiselectAround
{
    /**
     * @param Multiselect $subject
     * @param callable $proceed
     * @return array|string|string[]|null
     */
    public function aroundGetValues(
        Multiselect $subject,
        callable $proceed,
    ) {
        $value = $subject->getValue();
        if (!$value){
            $value = [];
        }

        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        return $value;
    }

}
