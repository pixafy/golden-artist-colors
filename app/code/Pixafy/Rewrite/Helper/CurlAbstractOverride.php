<?php

namespace Pixafy\Rewrite\Helper;

class CurlAbstractOverride extends \Pixafy\Boomi\Helper\CurlAbstract
{
    public function log($log_data)
    {
        if(is_array($log_data)){
            $log_data = serialize($log_data);
        }
        $writer = new \Zend_Log_Writer_Stream($this->getLogPath());
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info($log_data);
    }

}
