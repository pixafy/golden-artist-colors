<?php
/** This observer currently not used. TODO: remove observer and commented reference in events.xml **/
namespace Pixafy\BoomiPricing\Observer\Pricing;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Session;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\Request\Http;

class QuoteLoadAfter implements ObserverInterface
{

    private $checkoutSession;
    protected $request;
    private $pricing;
    private $messageManager;
    protected $resultRedirect;
    protected $url;
    protected $responseFactory;
    protected $area;

    const RESPONSE_ATTRIBUTE_MESSAGE    = "condition_message";
    const RESPONSE_ATTRIBUTE_CODE       = "condition_code";
    const RESPONSE_ATTRIBUTE_PRICE      = "price";

    public function __construct(
        Session $checkoutSession,
        Http $request,
        ManagerInterface $messageManager,
        \Magento\Framework\App\State $area,
        \Pixafy\BoomiPricing\Helper\CurlPricing $pricing,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResponseFactory $responseFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->pricing = $pricing;
        $this->messageManager = $messageManager;
        $this->request = $request;
        $this->url = $url;
        $this->area = $area;
        $this->responseFactory = $responseFactory;
    }

    public function execute(Observer $observer)
    {
        if($this->pricing->isB2BEnabled() || $this->pricing->isB2CEnabled()) {
            if ($this->request->getModuleName() == "checkout" || $this->area->getAreaCode() == \Magento\Framework\App\Area::AREA_WEBAPI_REST) {
                $quote = $observer->getQuote();
                try {
                    $cartData = $this->pricing->formatCartData($quote);
                    if(!$cartData){
                        return;
                    }
                    $prices = $this->pricing->getPrices($cartData);
                    foreach ($quote->getAllItems() as $item) {
                        $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                        if (array_key_exists($item->getProduct()->getSageId(), $prices)) {
                            $item_prices = $prices[$item->getProduct()->getSageId()];
                            $price = $item_prices[self::RESPONSE_ATTRIBUTE_PRICE];
                            if ($item_prices[self::RESPONSE_ATTRIBUTE_CODE] == "0") {
                                if($price != $item->getOriginalPrice()){
                                    $item->removeErrorInfosByParams(['code' => \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_CODE]);
                                    $item->setCustomPrice($price);
                                    $item->setOriginalCustomPrice($price);
                                    $item->getProduct()->setIsSuperMode(true);
                                }
                            } else {
                                if ($this->pricing->isProductCartAutodeleteEnabled()) {
                                    $message = $this->pricing->getSageItemErrorMessage($item, $item_prices[self::RESPONSE_ATTRIBUTE_MESSAGE]);
                                    $this->messageManager->addError($message);
                                    $item->delete();
                                }
                                else{
                                    $message = $this->pricing->getSageItemWarningMessage($item, $item_prices[self::RESPONSE_ATTRIBUTE_MESSAGE]);
                                    $this->messageManager->addWarning($message);
                                    $item->setCustomPrice(null);
                                    $item->setOriginalCustomPrice(null);
                                    $item->getProduct()->setIsSuperMode(false);
                                    $item->addErrorInfo('error',
                                        \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_CODE,
                                        \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_MESSAGE);
                                }
                            }

                        }
                    }
                    if ($this->request->getControllerName() == 'cart' && $this->request->getActionName() == 'add') {
                        return $this;
                    }
                    if ($this->request->getControllerName() == 'sidebar' && $this->request->getActionName() == 'removeItem') {
                        return $this;
                    }
                    $quote->setTotalsCollectedFlag(false)->collectTotals();
                    $quote->save();
                } catch (\Exception $e) {
                    foreach ($quote->getAllItems() as $item) {
                        $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                        $item->setCustomPrice(null);
                        $item->setOriginalCustomPrice(null);
                        $item->getProduct()->setIsSuperMode(false);
                        $item->addErrorInfo('error',
                            \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_CODE,
                            \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_MESSAGE);
                    }
                    $quote->setTotalsCollectedFlag(false)->collectTotals();
                    $quote->save();
                    if ($this->pricing->isSageDownLockEnabled()) {
                        $this->messageManager->addError($this->pricing->getSageDownError());
                    }
                    else{
                        $this->messageManager->addWarning($this->pricing->getSageDownWarning());
                    }
                    if ($this->request->getControllerName() == 'cart' && $this->request->getActionName() == 'index') {
                        return $this;
                    } else {
                        if ($this->pricing->isSageDownLockEnabled()) {
                            return $this->responseFactory->create()->setRedirect($this->url->getUrl("checkout/cart"))->sendResponse();
                        }
                        else{
                            return $this;
                        }
                    }
                }
            }
        }
    }
}

