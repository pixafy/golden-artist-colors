<?php

namespace Pixafy\BoomiPricing\Observer\Pricing;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class QuoteSubmit implements ObserverInterface
{
    private $pricing;

    public function __construct(
        \Pixafy\BoomiPricing\Helper\CurlPricing $pricing
    ) {
        $this->pricing = $pricing;
    }

    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        foreach ($quote->getAllItems() as $quoteItem) {
            $quoteItem = ($quoteItem->getParentItem() ? $quoteItem->getParentItem() : $quoteItem);
            foreach ($quoteItem->getErrorInfos() as $errorInfo){
                if($errorInfo['code'] == \Pixafy\BoomiPricing\Helper\CurlPricing::PRICE_SYNC_ERROR_CODE){
                    $order->setPriceSyncStatus(\Pixafy\BoomiPricing\Model\Pricing\Source\PriceSyncStatus::ERROR);
                    return $this;
                }
            }
        }
        $order->setPriceSyncStatus(\Pixafy\BoomiPricing\Model\Pricing\Source\PriceSyncStatus::SUCCESS);
        return $this;
    }
}

