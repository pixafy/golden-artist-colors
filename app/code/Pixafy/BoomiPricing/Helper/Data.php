<?php

namespace Pixafy\BoomiPricing\Helper;

class Data extends Config
{

    private $priceHelper;
    public function __construct(
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    )
    {
        $this->priceHelper = $priceHelper;
    }

    function isPriceCustomized($item){
        if($item->getOriginalPrice()){
            if($item->getOriginalCustomPrice() && round($item->getOriginalCustomPrice(), 2) < round($item->getOriginalPrice(), 2)){
                return true;
            }
        }
        return false;
    }

    function isNewPriceCustomized($item, $price){
        $product = $item->getProduct();
        if ($option = $item->getOptionByCode('simple_product')) {
            $product = $option->getProduct();
        }
        if($product && $this->priceHelper->currency($product->getPrice(), false, false))
        $base_price = $this->priceHelper->currency($product->getPrice(), false, false);
        if(isset($base_price)){
            if(round($price, 2) != round($base_price, 2)){
                return true;
            }
            else{
                return false;
            }
        }
        return true;
    }

}
