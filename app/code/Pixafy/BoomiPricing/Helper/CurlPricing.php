<?php

namespace Pixafy\BoomiPricing\Helper;

use Magento\Framework\HTTP\ClientInterface;
use Magento\Directory\Model\RegionFactory;
use Pixafy\Rewrite\Helper\CurlAbstractOverride;

class CurlPricing extends CurlAbstractOverride
{
    const PRICING_URL_NODE = 'ws/simple/getDynPricing';
    const PRODUCT_PLACEHOLDER = '%product_name%';
    const ERROR_PLACEHOLDER = '%error_message%';
    const PRICE_SYNC_ERROR_CODE = 'price_sync_failed';
    const PRICE_SYNC_ERROR_MESSAGE = 'Price Sync Failed';

    protected $log_file_name = "boomi_request_price.log";

    protected $pricingConfig;

    private $customerSession;
    private $companyRepository;
    private $jsonHelper;
    private $xmlParser;
    private $date;

    protected $current_company = null;
    protected $addressRepository;
    protected $regionFactory;
    protected $countryFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Company\Api\CompanyManagementInterface $companyRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Xml\Parser $xmlParser,
        RegionFactory $regionFactory,
        \Pixafy\Boomi\Helper\Config $boomiConfig,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Pixafy\BoomiPricing\Helper\Config $pricingConfig,
        ClientInterface  $client
    )
    {
        $this->customerSession = $customerSession;
        $this->addressRepository = $addressRepository;
        $this->companyRepository = $companyRepository;
        $this->jsonHelper = $jsonHelper;
        $this->xmlParser = $xmlParser;
        $this->pricingConfig = $pricingConfig;
        $this->regionFactory = $regionFactory;
        $this->countryFactory = $countryFactory;
        $this->date = $date;
        parent::__construct($boomiConfig, $client);
    }

    public function isB2BEnabled(){
        return $this->pricingConfig->isB2BPriceEnabled();
    }

    public function isB2CEnabled(){
        return $this->pricingConfig->isB2CPriceEnabled();
    }

    public function isProductCartAutodeleteEnabled(){
        return $this->pricingConfig->isProductCartAutodeleteEnabled();
    }

    public function getSageItemErrorMessage($item, $sage_error)
    {
        $message = $this->pricingConfig->getSageItemError();
        if ($message) {
            $message = str_replace(self::PRODUCT_PLACEHOLDER, $item->getProduct()->getName(), $message);
            $message = str_replace(self::ERROR_PLACEHOLDER, $sage_error, $message);
        }
        return $message ?: self::PRICE_SYNC_ERROR_MESSAGE;
    }

    public function getSageItemWarningMessage($item, $sage_error)
    {
        $message = $this->pricingConfig->getSageItemWarning();
        if ($message) {
            $message = str_replace(self::PRODUCT_PLACEHOLDER, $item->getProduct()->getName(), $message);
            $message = str_replace(self::ERROR_PLACEHOLDER, $sage_error, $message);
        }
        return $message ?: self::PRICE_SYNC_ERROR_MESSAGE;
    }

    public function getPrices($cartData){
        if($this->pricingConfig->isPriceLogEnabled()){
            $this->startExecTime();
            $this->log("Start price request");
        }
        $this->postPriceRequest(self::PRICING_URL_NODE, $cartData);
        if($this->pricingConfig->isPriceLogEnabled()){
            $this->endExecTime();
            $log_data = $this->prepareLogData($cartData);
            $this->log($log_data);
        }
        $result = [];
        if($this->isRequestSuccessful()) {
            return $this->prepareResponse();
        }else{
            throw new \Magento\Framework\Exception\LocalizedException(__("Boomi Price Listener is down."));
        }
    }

    protected function prepareResponse(){
        if($this->config->isBoomiWebServiceEnabled()){
            return $this->prepareResponseBoomi();
        }
        else{
            return $this->prepareResponseSage();
        }
    }

    protected function prepareResponseBoomi(){
        $result = [];
        $sage_price_data = $this->jsonHelper->jsonDecode($this->client->getBody());
        if (array_key_exists("items", $sage_price_data)) {
            foreach ($sage_price_data["items"] as $item) {
                $result[$item['sku']][$item['uom']] = array(
                    'price' => $item['price'],
                    'condition_code' => $item['condition_code'],
                    'condition_message' => isset($item['condition_message']) ? $item['condition_message'] : ""
                );
            }
        }
        return $result;
    }

    protected function prepareResponseSage()
    {
        $result = [];
        $xmlArray = $this->xmlParser->loadXML($this->client->getBody())->xmlToArray();
        $resultJson = false;
        $status = false;
        $messages = false;
        if(isset($xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['resultXml']['_value'])){
            $resultJson = $xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['resultXml']['_value'];
        }
        if(isset($xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['status']['_value'])){
            $status = $xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['status']['_value'];
        }
        if(isset($xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['messages']['_value'])){
            $messages = $xmlArray['soapenv:Envelope']['soapenv:Body']['wss:runResponse']['_value']['runReturn']['_value']['messages']['_value'];
        }

        if (!$status){
            if(isset($messages['message'])){
                throw new \Magento\Framework\Exception\LocalizedException(__("Sage error occured:".$messages['message']));
            }
            else{
                throw new \Magento\Framework\Exception\LocalizedException(__("Sage error occured."));
            }

        }

        $sage_price_data = $this->jsonHelper->jsonDecode($resultJson);
        if (array_key_exists("GRP2", $sage_price_data)) {
            foreach ($sage_price_data["GRP2"] as $item) {
                $result[$item['PITMREF']][$item['PUOM']] = array(
                    'price' => $item['PNETPRIUNT'],
                    'condition_code' => $item['PRETCODE'],
                    'condition_message' => isset($item['PRETMSG']) ? $item['PRETMSG'] : "",
                    'stopsale_code' => $item['PSTOPSALE'],
                    'restriction_message' => isset($item['PSTOPREA']) ? $item['PSTOPREA'] : ""
                );
            }
        }
        return $result;
    }

    protected function prepareLogData($cartData){
        return array(
            'execution_time' => $this->getExecTime(),
            'request_json' => $cartData,
            'response_code' => $this->client->getStatus(),
            'response' => $this->client->getBody()
        );
    }

    public function isSageDownLockEnabled(){
        return $this->pricingConfig->isSageDownLockEnabled();
    }

    public function getSageDownError(){
        return $this->pricingConfig->getSageDownError();
    }

    public function getSageDownWarning(){
        return $this->pricingConfig->getSageDownWarning();
    }

    public function formatCartData($quote){
        if($this->pricingConfig->isBoomiWebServiceEnabled()){
            return $this->formatBoomiCartData($quote);
        }
        else{
            return $this->formatSageCartData($quote);
        }
    }

    public function formatBoomiCartData($quote){
        $companySageId = $this->getCompanySageId();
        if(!$companySageId){
            return false;
        }
        if($this->isSageCompany()&&!$this->isB2BEnabled()){
            return false;
        }
        if(!$this->isSageCompany()&&!$this->isB2CEnabled()){
            return false;
        }

        $general = $this->prepareGeneralDataBoomi($quote);
        $result_items = $this->prepareItemsDataBoomi($quote);

        $result = new \Magento\Framework\DataObject();
        $result->setGeneral($general);
        $result->setItems($result_items);
        $encodedData = $this->jsonHelper->jsonEncode($result);
        return $encodedData;
    }

    public function formatSageCartData($quote){
        $companySageId = $this->getCompanySageId();
        if(!$companySageId){
            return false;
        }
        if($this->isSageCompany()&&!$this->isB2BEnabled()){
            return false;
        }
        if(!$this->isSageCompany()&&!$this->isB2CEnabled()){
            return false;
        }

        $general = $this->prepareGeneralDataSage($quote);
        $result_items = $this->prepareItemsDataSage($quote);

        $result = new \Magento\Framework\DataObject();
        $result->setData('GRP1', $general);
        $result->setData('GRP2', $result_items);
        $encodedData = $this->jsonHelper->jsonEncode($result);
        return $encodedData;
    }

    function isSageCompany(){
        $company = $this->getCurrentCompany();
        return ($company&&$company->getSageId())?true:false;
    }

    function getCurrentCompany(){
        if($this->customerSession->getCustomer()->getId()&&!$this->current_company){
            $this->current_company = $this->companyRepository->getByCustomerId($this->customerSession->getCustomer()->getId());
        }
        return $this->current_company;
    }

    function getCompanySageId(){
        $companySageId = null;
        $company = $this->getCurrentCompany();
        if($company&&$company->getSageId()){
            $companySageId = $company->getSageId();
        }
        if(!$companySageId){
            $companySageId = $this->pricingConfig->getB2CCustomerSageId();
        }
        return $companySageId;
    }

    function prepareGeneralDataBoomi($quote){
        if($quote->getCustomer() && $quote->getCustomer()->getDefaultShipping()){
            $address = $this->addressRepository->getById($quote->getCustomer()->getDefaultShipping());
        }
        if(isset($address) && $address->getCustomAttribute('sage_shipment_site')){
            $shipmentSite = $address->getCustomAttribute('sage_shipment_site')->getValue();
        }
        else{
            $shipmentSite = $this->pricingConfig->getDefaultSageSite();
        }
        $general = [
            'sales_site' => $shipmentSite,
            'stock_site' => $shipmentSite,
            'sage_company_id' => $this->getCompanySageId(),
            'date' => $this->date->gmtDate('Ymd'),
            'currency' => $quote->getQuoteCurrencyCode(),
            'use_tax' => Config::PRICE_SYNC_B2B_USE_TAX
        ];
        return $general;
    }

    function prepareItemsDataBoomi($quote){
        $result_items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $item = ($item->getParentItem() ? $item->getParentItem() : $item);
            $product = $item->getProduct();
            if ($option = $item->getOptionByCode('simple_product')) {
                $product = $option->getProduct();
            }
            $result_items[] = [
                'sage_id' => $product->getSageId(),
                'sku' => $product->getSageId(),
                'qty' => $item->getQty(),
                'price' => $item->getPrice(),
                'uom' => $item->getProduct()->getBoomiUnitOfMeasure()?$item->getProduct()->getBoomiUnitOfMeasure():$this->pricingConfig->getDefaultUOM()
            ];
        }
        return $result_items;
    }

    function prepareGeneralDataSage($quote){
        if($quote->getCustomer() && $quote->getCustomer()->getDefaultShipping()){
            $address = $this->addressRepository->getById($quote->getCustomer()->getDefaultShipping());
        }
        if(isset($address) && $address->getCustomAttribute('sage_shipment_site')){
            $shipmentSite = $address->getCustomAttribute('sage_shipment_site')->getValue();
        }
        else{
            $shipmentSite = $this->pricingConfig->getDefaultSageSite();
        }
        if($quote->getShippingAddress()){
            $shipping_address = $quote->getShippingAddress()->getData();
            //var_dump($shipping_address); exit;
            if(isset($shipping_address['region_id']) && $shipping_address['region_id']){
                $region = $this->regionFactory->create()->load($shipping_address['region_id']);
                $region = $region -> getCode();
            }
            else{
                $region = $shipping_address['region'];
            }
            if(isset($shipping_address['postcode'])){
                $postcode = $shipping_address['postcode'];
            }
            else{
                $postcode = '';
            }
            if(isset($shipping_address['city'])){
                $city = $shipping_address['city'];
            }
            else{
                $city = '';
            }
            if (isset($shipping_address['country_id'])){
                $country = $shipping_address['country_id'];
                $country = $this->countryFactory->create()->loadByCode($country);
                $country = $country->getData('iso3_code');
            }
            else{
                $country = 'USA';
            }
        }

        $general = [
            'PSALFCY' => $shipmentSite,
            'PSTOFCY' => $shipmentSite,
            'PBPCORD' => $this->getCompanySageId(),
            'PDAT' => $this->date->gmtDate('Ymd'),
            'PCUR' => $quote->getQuoteCurrencyCode(),
            'PEXCLTAX' => Config::PRICE_SYNC_B2B_USE_TAX,
            'PCOUNTRY' => $country,
            'PSTATE' => $region,
            'PPOSCOD' => $postcode,
            'PCITY' => $city
        ];
        return $general;
    }

    function prepareItemsDataSage($quote){
        $result_items = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $item = ($item->getParentItem() ? $item->getParentItem() : $item);
            $product = $item->getProduct();
            if ($option = $item->getOptionByCode('simple_product')) {
                $product = $option->getProduct();
            }
            $result_items[] = [
                'PITMREF' => $product->getSageId(),
                'PQTY' => $item->getQty(),
                'PUOM' => $product->getBoomiUnitOfMeasure()?$product->getBoomiUnitOfMeasure():$this->pricingConfig->getDefaultUOM()
            ];
        }
        return $result_items;
    }

}
