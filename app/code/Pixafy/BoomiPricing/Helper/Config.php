<?php

namespace Pixafy\BoomiPricing\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Config extends \Pixafy\Boomi\Helper\Config
{
    const PRICE_SYNC_B2B_PRICE_ENABLE                   = 'erpcommerce_catalog/price_sync/b2b_price_enable';
    const PRICE_SYNC_B2C_PRICE_ENABLE                   = 'erpcommerce_catalog/price_sync/b2c_price_enable';
    const PRICE_SYNC_PRODUCT_LIST_PRICE_DISABLE         = 'erpcommerce_catalog/price_sync/product_list_price_disable';
    const PRICE_SYNC_PRODUCT_PAGE_PRICE_DISABLE         = 'erpcommerce_catalog/price_sync/product_page_price_disable';
    const PRICE_SYNC_PRODUCT_CUSTOM_PRICE_PROMO_DISABLE = 'erpcommerce_catalog/price_sync/disable_custom_price_promotions';
    const PRICE_SYNC_PRODUCT_CART_AUTODELETE            = 'erpcommerce_catalog/price_sync/product_cart_autodelete';
    const PRICE_SYNC_DEFAULT_UOM                        = 'erpcommerce_catalog/price_sync/default_uom';
    const PRICE_SYNC_SAGE_ITEM_ERROR                    = 'erpcommerce_catalog/price_sync/sage_item_error';
    const PRICE_SYNC_SAGE_ITEM_WARNING                  = 'erpcommerce_catalog/price_sync/sage_item_warning';
    const PRICE_SYNC_SAGE_DOWN_LOCK                     = 'erpcommerce_catalog/price_sync/sage_down_lock';
    const PRICE_SYNC_SAGE_DOWN_ERROR                    = 'erpcommerce_catalog/price_sync/sage_down_error';
    const PRICE_SYNC_SAGE_DOWN_WARNING                  = 'erpcommerce_catalog/price_sync/sage_down_warning';
    const PRICE_SYNC_PRICE_LOG_ENABLE                   = 'erpcommerce_catalog/price_sync/price_log_enable';
    const PRICE_SYNC_ENABLE_PRICE_MESSAGE               = 'erpcommerce_catalog/price_sync/enable_price_message';
    const PRICE_SYNC_PRICE_MESSAGE                      = 'erpcommerce_catalog/price_sync/price_message';
    const PRICE_SYNC_DEFAULT_SAGE_SITE                  = 'erpcommerce_catalog/price_sync/default_sage_site';

    const PRICE_SYNC_B2B_USE_TAX = "Y";
    
    public function isB2BPriceEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_B2B_PRICE_ENABLE);
    }
    
    public function isB2CPriceEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_B2C_PRICE_ENABLE);
    }
    
    public function isProductListPriceDisable(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_PRODUCT_LIST_PRICE_DISABLE);
    }
    
    public function isProductPagePriceDisable(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_PRODUCT_PAGE_PRICE_DISABLE);
    }
    
    public function isCustomPricePromotionsDisabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_PRODUCT_CUSTOM_PRICE_PROMO_DISABLE);
    }
    
    public function isProductCartAutodeleteEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_PRODUCT_CART_AUTODELETE);
    }
    
    public function getDefaultUOM(){
        return $this->getConfigValue(self::PRICE_SYNC_DEFAULT_UOM);
    }
    
    public function getSageItemError(){
        return $this->getConfigValue(self::PRICE_SYNC_SAGE_ITEM_ERROR);
    }
    
    public function getSageItemWarning(){
        return $this->getConfigValue(self::PRICE_SYNC_SAGE_ITEM_WARNING);
    }
    
    public function isSageDownLockEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_SAGE_DOWN_LOCK);
    }
    
    public function getSageDownError(){
        return $this->getConfigValue(self::PRICE_SYNC_SAGE_DOWN_ERROR);
    }
    
    public function getSageDownWarning(){
        return $this->getConfigValue(self::PRICE_SYNC_SAGE_DOWN_WARNING);
    }
    
    public function isPriceLogEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_PRICE_LOG_ENABLE);
    }
    
    public function isPriceMessageEnabled(){
        return (bool) $this->getConfigValue(self::PRICE_SYNC_ENABLE_PRICE_MESSAGE);
    }

    public function getPriceMessage(){
        return $this->getConfigValue(self::PRICE_SYNC_PRICE_MESSAGE);
    }
    
    public function getDefaultSageSite()
    {
        return $this->getConfigValue(self::PRICE_SYNC_DEFAULT_SAGE_SITE);
    }
}
