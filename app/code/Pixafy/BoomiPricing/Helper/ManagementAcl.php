<?php

namespace Pixafy\BoomiPricing\Helper;

use RuntimeException;
use Magento\Framework\App\Request\Http;

class ManagementAcl
{
    
    private $config;
    private $request;

    public function __construct(Config $config, Http $request)
    {
        $this->config = $config;
        $this->request = $request;
    }

    private function isProductPage(){
        $moduleName = $this->request->getModuleName();
        $controller = $this->request->getControllerName();
        $action     = $this->request->getActionName();
        if($moduleName == "catalog" && $controller == "product" && $action == "view"){
            return true;
        }
        return false;
    }

    public function isPriceDisabled(){
        if($this->isProductPage()){
            if($this->config->isProductPagePriceDisable()){
                return true;
            }
        }
        else{
            if($this->config->isProductListPriceDisable()){
                return true;
            }
        }
        return false;
    }

}
