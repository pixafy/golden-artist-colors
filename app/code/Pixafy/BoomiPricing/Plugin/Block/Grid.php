<?php

namespace Pixafy\BoomiPricing\Plugin\Block;

use Magento\Checkout\Block\Cart;
use Pixafy\BoomiPricing\Helper\CurlPricing;

/**
 * Plugin class to add restriction message to items if pagination is going to display
 */
class Grid
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * @param Cart $cart
     */
    public function __construct(
        Cart $cart
    ) {
        $this->cart = $cart;
    }

    /**
     * @param $subject
     * @param $paginationItems
     * @return mixed
     */
    public function afterGetItems($subject, $paginationItems)
    {
        if(count($paginationItems) < $this->cart->getItemsCount()) {
            $cartItems = $this->cart->getItems();
            foreach ($paginationItems as $paginationItem) {
                foreach ($cartItems as $cartItem) {
                    if ($paginationItem->getSku() == $cartItem->getSku() && $cartItem->getMessage()) {
                        $paginationItem->addErrorInfo('error',
                            CurlPricing::PRICE_SYNC_ERROR_CODE,
                            $cartItem->getMessage());
                        break;
                    }
                }
            }
        }
        return $paginationItems;
    }
}
