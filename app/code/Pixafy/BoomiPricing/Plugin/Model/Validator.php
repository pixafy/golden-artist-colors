<?php
namespace Pixafy\BoomiPricing\Plugin\Model;

class Validator{
    
    protected $pricingConfig;
    protected $pricingHelper;

    public function __construct(
        \Pixafy\BoomiPricing\Helper\Config $pricingConfig,
        \Pixafy\BoomiPricing\Helper\Data $pricingHelper
    ){
        $this->pricingConfig = $pricingConfig;
        $this->pricingHelper = $pricingHelper;
    }
    
    public function afterCanApplyDiscount($subject, $result, $item)
    {
        if($result){
            if($this->pricingConfig->isCustomPricePromotionsDisabled()&&$this->pricingHelper->isPriceCustomized($item)){
                $result = false;
            }
        }
        
        return $result;
    }
    
}