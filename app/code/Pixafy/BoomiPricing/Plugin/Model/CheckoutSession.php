<?php

namespace Pixafy\BoomiPricing\Plugin\Model;

use Magento\Framework\App\ResponseFactory;
use Magento\Framework\App\State;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\UrlInterface;
use Pixafy\BoomiPricing\Helper\Config;
use Pixafy\BoomiPricing\Helper\CurlPricing;
use Pixafy\BoomiPricing\Helper\Data;

class CheckoutSession
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var CurlPricing
     */
    private $pricing;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var State
     */
    protected $area;

    /**
     * @var Config
     */
    protected $pricingConfig;

    /**
     * @var Data
     */
    protected $pricingHelper;

    protected $quote;

    const RESPONSE_ATTRIBUTE_MESSAGE    = "condition_message";
    const RESPONSE_ATTRIBUTE_CODE       = "condition_code";
    const STOPSALE_ATTRIBUTE_CODE       = "stopsale_code";
    const STOPSALE_ATTRIBUTE_MESSAGE    = "restriction_message";
    const RESPONSE_ATTRIBUTE_PRICE      = "price";

    /**
     * @param Http $request
     * @param ManagerInterface $messageManager
     * @param State $area
     * @param CurlPricing $pricing
     * @param UrlInterface $url
     * @param ResponseFactory $responseFactory
     * @param Config $pricingConfig
     * @param Data $pricingHelper
     */
    public function __construct(
        Http $request,
        ManagerInterface $messageManager,
        State $area,
        CurlPricing $pricing,
        UrlInterface $url,
        ResponseFactory $responseFactory,
        Config $pricingConfig,
        Data $pricingHelper
    ) {
        $this->pricing = $pricing;
        $this->messageManager = $messageManager;
        $this->request = $request;
        $this->url = $url;
        $this->area = $area;
        $this->responseFactory = $responseFactory;
        $this->pricingConfig = $pricingConfig;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * @param $subject
     * @param $quote
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetQuote($subject, $quote)
    {
        if(!$this->quote){
            $this->quote = $quote;
            if($quote->getAllItems() && ($this->pricing->isB2BEnabled() || $this->pricing->isB2CEnabled())) {
                if ($this->request->getModuleName() == "checkout" || $this->area->getAreaCode() == \Magento\Framework\App\Area::AREA_WEBAPI_REST) {
                    try {
                        $cartData = $this->pricing->formatCartData($quote);
                        if(!$cartData){
                            return $quote;
                        }
                        $prices = $this->pricing->getPrices($cartData);

                        $errorItems = 0;
                        foreach ($quote->getAllVisibleItems() as $item) {
                            $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                            $product = $item->getProduct();
                            if ($option = $item->getOptionByCode('simple_product')) {
                                $product = $option->getProduct();
                            }

                            if (array_key_exists($product->getSageId(), $prices)) {
                                $item_prices = $prices[$product->getSageId()];
                                $uom = $product->getBoomiUnitOfMeasure()?$product->getBoomiUnitOfMeasure():$this->pricingConfig->getDefaultUOM();
                                $price = $item_prices[$uom][self::RESPONSE_ATTRIBUTE_PRICE];

                                if (array_key_exists($uom, $item_prices)) {
                                    if ($item_prices[$uom][self::RESPONSE_ATTRIBUTE_CODE] == "0") {
                                        if($this->pricingHelper->isNewPriceCustomized($item, $price)){
                                            $item->removeErrorInfosByParams(['code' => CurlPricing::PRICE_SYNC_ERROR_CODE]);
                                            $item->setOriginalCustomPrice($price);
                                            $item->getProduct()->setIsSuperMode(true);
                                        } else {
											$item->setCustomPrice(null);
                                            $item->setOriginalCustomPrice(null);
											$item->getProduct()->setIsSuperMode(false);
										}
                                    } else {
                                        if ($this->pricing->isProductCartAutodeleteEnabled()) {
                                            $message = $this->pricing->getSageItemErrorMessage($item, $item_prices[$uom][self::RESPONSE_ATTRIBUTE_MESSAGE]);
                                            $this->messageManager->addErrorMessage($message);
                                            $item->delete();
                                        } else {
                                            $message = $this->pricing->getSageItemWarningMessage($item, $item_prices[$uom][self::RESPONSE_ATTRIBUTE_MESSAGE]);
                                            //$this->messageManager->addWarning($message);
                                            $item->setCustomPrice(null);
                                            $item->setOriginalCustomPrice(null);
                                            $item->getProduct()->setIsSuperMode(false);
                                                $item->addErrorInfo('error',
                                                CurlPricing::PRICE_SYNC_ERROR_CODE,
                                                $item_prices[$uom][self::RESPONSE_ATTRIBUTE_MESSAGE].". ".$item_prices[$uom][self::STOPSALE_ATTRIBUTE_MESSAGE]);
                                            $errorItems++;
                                        }
                                    }
                                }
                            }
                        }

                        $quote->setHasError(false);
                        if ($errorItems > 0) {
                            $quote->setHasError(true);
                        }

                        if ($this->request->getControllerName() == 'cart' && $this->request->getActionName() == 'add') {
                            return $quote;
                        }
                        if ($this->request->getControllerName() == 'sidebar' && $this->request->getActionName() == 'removeItem') {
                            return $quote;
                        }
                        $quote->setTotalsCollectedFlag(false)->collectTotals();
                        $quote->save();
                    } catch (\Exception $e) {
                        $this->pricing->log("Exception: " . $e->getMessage());
                        foreach ($quote->getAllItems() as $item)
                        {
                            $item = ($item->getParentItem() ? $item->getParentItem() : $item);
                            $item->setCustomPrice(null);
                            $item->setOriginalCustomPrice(null);
                            $item->getProduct()->setIsSuperMode(false);
                            $item->addErrorInfo('error',
                                CurlPricing::PRICE_SYNC_ERROR_CODE,
                                CurlPricing::PRICE_SYNC_ERROR_MESSAGE);
                            $quote->setHasError(true);
                        }
                        $quote->setTotalsCollectedFlag(false)->collectTotals();
                        $quote->save();

                        if ($this->pricing->isSageDownLockEnabled()) {
                            $this->messageManager->addErrorMessage($this->pricing->getSageDownError());
                        } else {
                            $this->messageManager->addWarningMessage($this->pricing->getSageDownWarning());
                        }

                        if ($this->request->getControllerName() == 'cart' && $this->request->getActionName() == 'index') {
                            return $quote;
                        } else {
                            if ($this->pricing->isSageDownLockEnabled()) {
                                return $this->responseFactory->create()->setRedirect($this->url->getUrl("checkout/cart"))->sendResponse();
                            } else {
                                return $quote;
                            }
                        }
                    }
                }
            }
        }
        return $quote;
    }
}
