<?php

namespace Pixafy\BoomiPricing\Plugin\Pricing;

use Pixafy\BoomiPricing\Helper\ManagementAcl;
use Magento\Framework\View\LayoutInterface;

class FinalPriceBox
{
    private $managementAcl;
    private $layout;

    const PRICE_MESSAGE_BLOCK = 'Pixafy\BoomiPricing\Block\Price\Message';
    const PRICE_MESSAGE_TEMPLATE = 'Pixafy_BoomiPricing::catalog/product/price/message.phtml';
    
    public function __construct(ManagementAcl $managementAcl, LayoutInterface $layout)
    {
        $this->layout = $layout;
        $this->managementAcl = $managementAcl;
    }

    function aroundToHtml($subject, callable $proceed)
    {
        if($this->managementAcl->isPriceDisabled()){
            return '';
        }
        $proceed = $proceed();
        $messageBlockContent = $this->getPriceMessageBlock()->toHtml();
        return $proceed.$messageBlockContent;
    }
    
    function getPriceMessageBlock(){
        $block = $this->layout
            ->createBlock(self::PRICE_MESSAGE_BLOCK)
            ->setTemplate(self::PRICE_MESSAGE_TEMPLATE);
        return $block;
    }
}
