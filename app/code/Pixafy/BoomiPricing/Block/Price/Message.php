<?php

namespace Pixafy\BoomiPricing\Block\Price;

use Magento\Framework\View\Element\Template;
use Pixafy\BoomiPricing\Helper\Config;

class Message extends Template
{
    
    protected $config;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Config $config,
        array $data = []
    ) {
        $this->config = $config;
        parent::__construct($context, $data);
    }

    function isMessageEnabled(){
        return $this->config->isPriceMessageEnabled();
    }
    
    function getPriceMessage(){
        return $this->config->getPriceMessage();
    }
    
    
}
