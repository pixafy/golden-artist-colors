<?php
/**
 * Pixafy_BoomiPricing extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Pixafy
 * @package   Pixafy_BoomiPricing
 * @copyright Copyright (c) 2019
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Pixafy\BoomiPricing\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Module\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->installEntities($setup, $context);
        $setup->endSetup();
    }

    private function installEntities(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $tableNames = ['sales_order', 'sales_order_grid'];
        foreach ($tableNames as $tableName) {
            $this->installEntityTable($tableName, $setup, $context);
        }
    }

    private function installEntityTable($tableName, SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $table = $setup->getTable($tableName);
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'price_sync_status',
            [
                'type'     => Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => true,
                'default'  => null,
                'after'    => 'sage_id',
                'comment'  => 'Sage Price Sync Status',
            ]
        );
    }

}
