<?php

namespace Pixafy\BoomiPricing\Model\Pricing\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PriceSyncStatus extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    const ERROR   = 0;
    const SUCCESS = 1;
    const WARNING = 2;
    const PROCESSING = 3;

    public function toOptionArray()
    {
        $options = [];
        foreach ($this->getOptionArray() as $value => $label) {
            $options[] = compact('value', 'label');
        }
        return $options;
    }

    private function getOptionArray()
    {
        return [
            self::ERROR   => __('Error'),
            self::SUCCESS => __('Success')
        ];
    }
    public function getAllOptions()
    {
        $result = [];
        foreach ($this->getOptionArray() as $k => $v) {
            $result[] = ['value' => $k, 'label' => $v];
        }

        return $result;
    }
}
