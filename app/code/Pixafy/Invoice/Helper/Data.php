<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Helper;

use \Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**#@+
     * Keys
     */
    const CONFIG_INVOICE_NOTE = 'pixafy_config_invoice/general/note';
    /**#@-*/

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Return config value by path
     *
     * @param string $path
     * @param null $store
     *
     * @return string
     */
    protected function getConfigValueByPath($path, $store = null)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Return invoice note
     *
     * @param null $store
     *
     * @return string
     */
    public function getInvoiceNote($store = null)
    {
        return $this->getConfigValueByPath(self::CONFIG_INVOICE_NOTE, $store);
    }

}
