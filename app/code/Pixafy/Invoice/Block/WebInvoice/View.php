<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Block\WebInvoice;

class View extends \Pixafy\BoomiInvoice\Block\WebInvoice\View
{
    /**
     * @var \Pixafy\Invoice\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Pixafy\Invoice\Helper\Data $dataHelper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Pixafy\Invoice\Helper\Data                      $dataHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry                      $registry,
        array                                            $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $registry, $data);
    }

    /**
     * Return invoice note
     *
     * @return string
     */
    public function getInvoiceNote()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->dataHelper->getInvoiceNote($storeId);
    }
}
