<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Block\Order\PrintOrder;

/**
 * Sales order details block
 *
 * @api
 */
class Invoice extends \Magento\Sales\Block\Order\PrintOrder\Invoice
{
    /**
     * @var \Pixafy\Invoice\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Pixafy\Invoice\Helper\Data $dataHelper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Pixafy\Invoice\Helper\Data                      $dataHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry                      $registry,
        \Magento\Payment\Helper\Data                     $paymentHelper,
        \Magento\Sales\Model\Order\Address\Renderer      $addressRenderer,
        array                                            $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $registry, $paymentHelper, $addressRenderer, $data);
    }

    /**
     * Return invoice note
     *
     * @return string
     */
    public function getInvoiceNote()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->dataHelper->getInvoiceNote($storeId);
    }

}
