<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Block\Order\Email\Invoice;

use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Sales Order Email Invoice items
 */
class Items extends \Magento\Sales\Block\Order\Email\Invoice\Items
{
    /**
     * @var \Pixafy\Invoice\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Pixafy\Invoice\Helper\Data $dataHelper
     * @param Context $context
     * @param array $data
     * @param OrderRepositoryInterface|null $orderRepository
     * @param InvoiceRepositoryInterface|null $invoiceRepository
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Pixafy\Invoice\Helper\Data                $dataHelper,
        Context                                    $context,
        array                                      $data = [],
        ?OrderRepositoryInterface                  $orderRepository = null,
        ?InvoiceRepositoryInterface                $invoiceRepository = null
    )
    {
        $this->storeManager = $storeManager;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * Return invoice note
     *
     * @return string
     */
    public function getInvoiceNote()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->dataHelper->getInvoiceNote($storeId);
    }
}
