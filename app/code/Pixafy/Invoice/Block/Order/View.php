<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Block\Order;

/**
 * Sales order view block
 *
 * @api
 */
class View extends \Magento\Sales\Block\Order\View
{
    /**
     * @var \Pixafy\Invoice\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Pixafy\Invoice\Helper\Data $dataHelper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Pixafy\Invoice\Helper\Data                      $dataHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry                      $registry,
        \Magento\Framework\App\Http\Context              $httpContext,
        \Magento\Payment\Helper\Data                     $paymentHelper,
        array                                            $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $registry, $httpContext, $paymentHelper, $data);
    }

    /**
     * Return invoice note
     *
     * @return string
     */
    public function getInvoiceNote()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->dataHelper->getInvoiceNote($storeId);
    }

}
