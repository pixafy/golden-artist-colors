<?php
/**
 * @copyright Copyright © 2021 Pixafy. All rights reserved.
 * @author Hector Perez Hernandez <hperez@zircon.tech>
 */

namespace Pixafy\Invoice\Model\Order\Pdf;

use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection;

/**
 * Sales Order Invoice PDF model override
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Invoice extends \Magento\Sales\Model\Order\Pdf\Invoice
{
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;

    /**
     * @var \Pixafy\Invoice\Helper\Data
     */
    protected $dataHelper;

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     * @param \Pixafy\Invoice\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data                         $paymentData,
        \Magento\Framework\Stdlib\StringUtils                $string,
        \Magento\Framework\App\Config\ScopeConfigInterface   $scopeConfig,
        \Magento\Framework\Filesystem                        $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config                $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory         $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory          $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface   $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer          $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface           $storeManager,
        \Magento\Store\Model\App\Emulation                   $appEmulation,
        \Pixafy\Invoice\Helper\Data                          $dataHelper,
        array                                                $data = []
    )
    {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $data
        );
        $this->appEmulation = $appEmulation;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Return PDF document
     *
     * @param array|Collection $invoices
     * @return \Zend_Pdf
     */
    public function getPdf($invoices = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                $this->appEmulation->startEnvironmentEmulation(
                    $invoice->getStoreId(),
                    \Magento\Framework\App\Area::AREA_FRONTEND,
                    true
                );
                $this->_storeManager->setCurrentStore($invoice->getStoreId());
            }
            $page = $this->newPage();
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );
            /* Add document text and number */
            $this->insertDocumentNumber($page, __('Invoice # ') . $invoice->getIncrementId());
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $invoice);
            /* Add note */
            $this->addNote($page);
            if ($invoice->getStoreId()) {
                $this->appEmulation->stopEnvironmentEmulation();
            }
        }
        $this->_afterGetPdf();
        return $pdf;
    }

    /**
     * @param \Zend_Pdf_Page $page
     */
    protected function addNote(\Zend_Pdf_Page $page)
    {
        $storeId = $this->_storeManager->getStore()->getId();
        $note = $this->dataHelper->getInvoiceNote($storeId);
        $this->y -= 20;
        $this->_setFontRegular($page, 10);
        $this->drawTextArea($note, 25, 15, 129, $page);
    }

    /**
     * Puts text box to a page
     *
     * @param string $text
     * @param int $x
     * @param int $height
     * @param int $length
     * @param $page
     */
    public function drawTextArea($text, $x, $height, $length, $page)
    {
        if ($length != 0) {
            $text = wordwrap($text, $length, "\n", false);
        }
        $token = strtok($text, "\n");

        while ($token != false) {
            $page->drawText($token, $x, $this->y, 'UTF-8');
            $token = strtok("\n");
            $this->y -= $height;
        }
    }
}
