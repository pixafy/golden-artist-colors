/**
 * Pixafy_Boomi extension
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Pixafy
 * @package   Pixafy_Boomi
 * @copyright Copyright (c) 2023
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

define([
    'Magento_Ui/js/grid/columns/select',
], function (Select) {
    'use strict';

    return Select.extend({
        defaults: {
            bodyTmpl: "Pixafy_Boomi/grid/column/sync_status",
            tooltipMessageMaxLength: 500
        },

        /**
         *
         * @param record
         * @returns {string}
         */
        getFieldClass: function (record) {
            return 'sync-status-' + record[this.index];
        },

        /**
         *
         * @param record
         * @returns {string|*}
         */
        getTooltipMessage(record) {
            if (record.message && record.message.length > this.tooltipMessageMaxLength) {
                return record.message.substring(0, this.tooltipMessageMaxLength) + '...';
            }
            return record.message;
        }
    });
});
